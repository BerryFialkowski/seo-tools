# ~*~ encoding: utf-8 ~*~
import json
import redis


class Database(object):

    __instance__ = None

    @classmethod
    def instance(cls):
        if cls.__instance__ is None:
            cls.__instance__ = cls()
        return cls.__instance__

    def add_keyword(self, keyword):
        raise NotImplemented

    def add_root_keyword(self, root):
        raise NotImplemented

    def add_suggestion(self, keyword, suggestion):
        raise NotImplemented

    def add_task(self, task):
        raise NotImplemented

    def get_root_keywords(self):
        raise NotImplemented

    def get_suggestions(self, keyword):
        raise NotImplemented

    def get_task(self):
        raise NotImplemented


class RedisDatabase(Database):

    keyword_pk = 'keyword.pk'
    prefix_keyword = 'keyword'
    prefix_suggestion = 'suggestion'
    root_keywords = 'root_keywords'
    task_queue = 'tasks'
    task_suggestion_queue = 'task_suggestion'
    users_auth = 'users:{}:auth'

    def __init__(self, host='localhost', port=6379, db=0):
        self.rdb = redis.StrictRedis(host=host, port=port, db=db)

    @staticmethod
    def _make_case_insensitive(keyword):
        transformer = lambda char: '[{}{}]'.format(char.lower(), char.upper())
        return '*{}*'.format(''.join(map(transformer, keyword)))

    def add_root_keyword(self, root):
        self.rdb.sadd(self.root_keywords, root)

    def add_suggestion(self, keyword, suggestion):
        name = '{}:{}'.format(self.prefix_suggestion, keyword)
        self.rdb.sadd(name, suggestion)

    def add_task(self, task):
        encoded_task = json.dumps(task)
        self.rdb.lpush(self.task_queue, encoded_task)

    def add_task_suggestion(self, task):
        encoded_task = json.dumps(task)
        self.rdb.lpush(self.task_suggestion_queue, encoded_task)

    def check_auth(self, username, password):
        keys = self.rdb.keys(self.users_auth.format('*'))
        for key in keys:
            user = self.rdb.hgetall(key)
            if 'username' in user and 'password' in user \
                    and username == user['username'] \
                    and password == user['password']:
                return True
        return False

    def get_number_of_tasks(self):
        return self.rdb.llen(self.task_queue) \
            + self.rdb.llen(self.task_suggestion_queue)

    def get_root_keywords(self):
        return self.rdb.smembers(self.root_keywords)

    def get_suggestions(self, keyword):
        name = '{}:{}'.format(self.prefix_suggestion, keyword)
        return self.rdb.smembers(name)

    def get_task(self):
        queue, encoded_task = self.rdb.brpop(self.task_queue)
        if queue == self.task_queue:
            return json.loads(encoded_task)

    def get_task_suggestion(self):
        queue, encoded_task = self.rdb.brpop(self.task_suggestion_queue)
        if queue == self.task_suggestion_queue:
            return json.loads(encoded_task)

    def search_roots(self, keyword=None):
        keyword = keyword or ''
        keyword = self._make_case_insensitive(keyword)

        cursor, count = 0, 1000
        cursor, result = self.rdb.sscan(
            self.root_keywords, cursor, keyword, count)

        while cursor > 0:
            cursor, tail = self.rdb.sscan(
                self.root_keywords, cursor, keyword, count)
            result.extend(tail)

        return result


class Formatter(object):

    @staticmethod
    def get_suggestions(root_keyword, rdb):
        if isinstance(root_keyword, str):
            line = [unicode(root_keyword, 'utf-8')]
        else:
            line = [root_keyword]
        tail = set()
        suggestions = rdb.get_suggestions(root_keyword)
        while len(suggestions):
            new_suggestions = []
            for i, keyword in enumerate(suggestions):
                if keyword not in tail:
                    new_suggestions.extend(rdb.get_suggestions(keyword))
                    tail.add(unicode(keyword, 'utf-8'))
            suggestions = new_suggestions
        line.extend(tail)
        return line

    @classmethod
    def get_arrays(cls, data=None):
        rdb = RedisDatabase.instance()
        data = data or dict()

        if 'keyword' in data:
            roots = [data.get('keyword', '*')]
        else:
            roots = rdb.get_root_keywords()

        data = []
        for i, root in enumerate(roots):
            data.append(cls.get_suggestions(root, rdb))

        return data

    @classmethod
    def get_dict(cls, data=None):
        res = [dict(root=row[0], suggestions=row[1:])
               for row in cls.get_arrays(data)]
        return res
