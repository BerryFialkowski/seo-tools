#!/bin/sh

# runit service supervision
cp -avr sv/scraper /etc/sv
chmod -v u+x /etc/sv/scraper/run
ln -v -s /etc/sv/scraper /etc/service/scraper

pip install -r requirements.txt
