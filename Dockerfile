FROM phusion/passenger-customizable:0.9.17
MAINTAINER koutoftimer <koutoftimer@gmail.com>

# Use baseimage-docker's init process.
CMD ["/sbin/my_init"]

# Build system and git.
RUN /pd_build/utilities.sh

# Python support.
RUN /pd_build/python.sh
RUN apt-get install -y python-pip

# Opt-in for Redis if you're using the 'customizable' image.
RUN /pd_build/redis.sh

# Nginx and Passenger are disabled by default. Enable them like so:
RUN rm -f /etc/service/nginx/down
RUN rm /etc/nginx/sites-enabled/default
ADD nginx/scraper.conf /etc/nginx/sites-enabled/scraper.conf

# Enable the Redis service.
RUN rm -f /etc/service/redis/down

# Enabling SSH
RUN rm -f /etc/service/sshd/down

# Regenerate SSH host keys. Passenger-docker does not contain any, so you
# have to do that yourself. You may also comment out this instruction; the
# init system will auto-generate one during boot.
RUN /etc/my_init.d/00_regen_ssh_host_keys.sh

RUN mkdir -p /home/app/scraper/public
WORKDIR /home/app/scraper/
EXPOSE 80 433 22 5000

# Add dependencies for libxml
RUN apt-get install -y python-dev libxml2-dev libxslt1-dev lib32z1-dev
